package com.cubearrow.controller;

import de.cubearrow.restview.services.ProblemService;
import org.springframework.boot.SpringApplication;

public class Main {
    public static void main(String[] args) {
        SpringApplication.run(ProblemService.class);
    }
}
